package app.subbu;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import app.subbu.repository.local.AppDatabase;
import app.subbu.repository.local.entities.CustomerEntity;
import app.subbu.repository.local.entities.TableMapEntity;

import static org.junit.Assert.assertTrue;

/**
 * Created by subramanyam.gurajada on 30.07.17.
 */

@RunWith(AndroidJUnit4.class)
public class EntityReadWriteTest {
    private AppDatabase db;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getTargetContext();
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase.class).allowMainThreadQueries().build();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void writeCustomerAndReadInTest() throws Exception {
        CustomerEntity entity = new CustomerEntity(0, "John", "Doe");
        db.customerDao().insertCustomer(entity);
        CustomerEntity customerEntity = db.customerDao().findById(0);
        assertTrue(entity.getId() == customerEntity.getId());
    }

    @Test
    public void writeTableMapAndReadInTest() throws Exception {
        TableMapEntity entity = new TableMapEntity(1, false);
        db.tableMapDao().insertTableMap(entity);
        TableMapEntity tableMapEntity = db.tableMapDao().findById(1);
        assertTrue(entity.getId() == tableMapEntity.getId() &&
        entity.isAvailable() == tableMapEntity.isAvailable());
    }
}
