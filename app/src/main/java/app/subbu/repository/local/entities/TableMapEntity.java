package app.subbu.repository.local.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by subramanyam.gurajada on 30.07.17.
 */

@Entity(tableName = "tablemap")
public class TableMapEntity implements Parcelable {

    @PrimaryKey
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "available")
    private boolean available;

    public TableMapEntity(int id, boolean available) {
        this.id = id;
        this.available = available;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeByte(this.available ? (byte) 1 : (byte) 0);
    }

    protected TableMapEntity(Parcel in) {
        this.id = in.readInt();
        this.available = in.readByte() != 0;
    }

    public static final Parcelable.Creator<TableMapEntity> CREATOR = new Parcelable.Creator<TableMapEntity>() {
        @Override
        public TableMapEntity createFromParcel(Parcel source) {
            return new TableMapEntity(source);
        }

        @Override
        public TableMapEntity[] newArray(int size) {
            return new TableMapEntity[size];
        }
    };
}
