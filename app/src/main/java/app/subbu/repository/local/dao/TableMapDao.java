package app.subbu.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import app.subbu.repository.local.entities.TableMapEntity;
import io.reactivex.Flowable;

/**
 * Created by subramanyam.gurajada on 30.07.17.
 */

@Dao
public interface TableMapDao {

    @Query("SELECT * FROM tablemap")
    Flowable<List<TableMapEntity>> getTableMapEntities();

    @Query("SELECT * FROM tablemap where id = :id")
    TableMapEntity findById(int id);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertTableMap(TableMapEntity tableMapEntity);

    @Update
    int updateTableMap(TableMapEntity... tableMapEntity);

    @Delete
    void deleteTableMap(TableMapEntity... tableMapEntity);

    @Query("DELETE FROM tablemap")
    void deleteAll();

    @Query("UPDATE tablemap set available=1")
    int clearAvailability();
}
