package app.subbu.repository.local;

import java.util.List;

import javax.inject.Inject;

import app.subbu.repository.local.entities.CustomerEntity;
import app.subbu.repository.local.entities.TableMapEntity;
import io.reactivex.Flowable;
import io.reactivex.Observable;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public class LocalRepository {

    private final AppDatabase appDatabase;

    @Inject
    public LocalRepository(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    public Flowable<List<CustomerEntity>> getCustomerEntities() {
        return appDatabase.customerDao().getCustomerEntities();
    }

    public void putCustomerEntity(CustomerEntity customerEntity) {
        appDatabase.customerDao().insertCustomer(customerEntity);
    }

    public Observable<Integer> updateCustomerEntity(CustomerEntity customerEntity) {
        return Observable.fromCallable(() -> appDatabase.customerDao().updateCustomers(customerEntity));
    }

    public Flowable<List<TableMapEntity>> getTableMapEntities() {
        return appDatabase.tableMapDao().getTableMapEntities();
    }

    public void putTableMapEntity(TableMapEntity tableMapEntity) {
        appDatabase.tableMapDao().insertTableMap(tableMapEntity);
    }

    public Observable<Integer> updateTableMapEntity(TableMapEntity tableMapEntity) {
        return Observable.fromCallable(() ->  appDatabase.tableMapDao().updateTableMap(tableMapEntity));
    }

    public Observable<Integer> clearTableReservations() {
        return Observable.fromCallable(() ->  appDatabase.tableMapDao().clearAvailability());
    }

    public Flowable<List<CustomerEntity>> searchCustomerEntities(String query) {
        return appDatabase.customerDao().searchCustomerEntities(query);
    }
}
