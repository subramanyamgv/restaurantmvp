package app.subbu.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import app.subbu.repository.local.entities.CustomerEntity;
import io.reactivex.Flowable;

/**
 * Created by subramanyam.gurajada on 30.07.17.
 */

@Dao
public interface CustomerDao {

    @Query("SELECT * FROM customer")
    Flowable<List<CustomerEntity>> getCustomerEntities();

    @Query("SELECT * FROM customer where id = :id")
    CustomerEntity findById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCustomer(CustomerEntity customerEntity);

    @Query("SELECT * FROM customer WHERE firstName LIKE '%' || :query || '%' OR lastName LIKE '%' || :query || '%'")
    Flowable<List<CustomerEntity>> searchCustomerEntities(String query);

    @Update
    int updateCustomers(CustomerEntity... customerEntities);

    @Delete
    void deleteCustomers(CustomerEntity... customerEntities);

    @Query("DELETE FROM customer")
    void deleteAllCustomers();
}
