package app.subbu.repository.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import app.subbu.repository.local.dao.CustomerDao;
import app.subbu.repository.local.dao.TableMapDao;
import app.subbu.repository.local.entities.CustomerEntity;
import app.subbu.repository.local.entities.TableMapEntity;

/**
 * Created by subramanyam.gurajada on 30.07.17.
 */
@Database(entities = {CustomerEntity.class, TableMapEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract CustomerDao customerDao();

    public abstract TableMapDao tableMapDao();
}
