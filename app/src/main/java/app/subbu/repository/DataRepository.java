package app.subbu.repository;

import java.util.List;

import javax.inject.Inject;

import app.subbu.mvp.model.Customer;
import app.subbu.repository.local.LocalRepository;
import app.subbu.repository.local.entities.CustomerEntity;
import app.subbu.repository.local.entities.TableMapEntity;
import app.subbu.repository.network.ApiRepository;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public class DataRepository implements Repository {

    private ApiRepository apiRepository;
    private LocalRepository localRepository;

    @Inject
    public DataRepository(ApiRepository apiRepository, LocalRepository localRepository) {
        this.apiRepository = apiRepository;
        this.localRepository = localRepository;
    }

    @Override
    public Flowable<List<Customer>> getCustomerList() {
        return apiRepository.getCustomerList();
    }

    @Override
    public Flowable<List<Boolean>> getTableMap() {
        return apiRepository.getTableMap();
    }

    @Override
    public Flowable<List<CustomerEntity>> getCustomerEntities() {
        return localRepository.getCustomerEntities();
    }

    public void putCustomer(CustomerEntity customerEntity) {
        localRepository.putCustomerEntity(customerEntity);
    }

    @Override
    public Flowable<List<TableMapEntity>> getTableMapEntities() {
        return localRepository.getTableMapEntities();
    }

    public void putTableMapEntity(TableMapEntity tableMapEntity) {
        localRepository.putTableMapEntity(tableMapEntity);
    }

    public void updateTableMapEntity(TableMapEntity tableMapEntity) {
        localRepository.updateTableMapEntity(tableMapEntity).subscribeOn(Schedulers.io()).subscribe();
    }

    public void clearTableReservations() {
        localRepository.clearTableReservations().subscribeOn(Schedulers.io()).subscribe();
    }

    public Flowable<List<CustomerEntity>> searchCustomerEntities(String query) {
        return localRepository.searchCustomerEntities(query);
    }
}