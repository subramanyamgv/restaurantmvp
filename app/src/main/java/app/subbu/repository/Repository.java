package app.subbu.repository;

import java.util.List;

import app.subbu.mvp.model.Customer;
import app.subbu.repository.local.entities.CustomerEntity;
import app.subbu.repository.local.entities.TableMapEntity;
import io.reactivex.Flowable;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public interface Repository {

    Flowable<List<Customer>> getCustomerList();

    Flowable<List<Boolean>> getTableMap();

    Flowable<List<CustomerEntity>> getCustomerEntities();

    Flowable<List<TableMapEntity>> getTableMapEntities();
}
