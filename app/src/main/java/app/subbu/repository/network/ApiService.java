package app.subbu.repository.network;

import java.util.List;

import app.subbu.BuildConfig;
import app.subbu.mvp.model.Customer;
import io.reactivex.Flowable;
import retrofit2.http.GET;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public interface ApiService {

    String CUSTOMER_LIST_END_POINT = BuildConfig.apiEndpointUrl + "quandoo-assessment/customer-list.json";
    String TABLE_MAP_LIST_END_POINT = BuildConfig.apiEndpointUrl + "quandoo-assessment/table-map.json";

    @GET(CUSTOMER_LIST_END_POINT)
    Flowable<List<Customer>> getCustomerList();

    @GET(TABLE_MAP_LIST_END_POINT)
    Flowable<List<Boolean>> getTableMap();
}
