package app.subbu.repository.network;

import java.util.List;

import javax.inject.Inject;

import app.subbu.mvp.model.Customer;
import io.reactivex.Flowable;
import retrofit2.Retrofit;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public class ApiRepository {

    private ApiService apiService;

    @Inject
    public ApiRepository(Retrofit retrofit) {
        this.apiService = retrofit.create(ApiService.class);
    }

    public Flowable<List<Customer>> getCustomerList() {
        return apiService.getCustomerList();
    }

    public Flowable<List<Boolean>> getTableMap() {
        return apiService.getTableMap();
    }
}
