package app.subbu.utils;

import android.text.format.DateFormat;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public class DateTimeUtils {

    public final static int MINUTE = 60;
    public final static String TIME_FORMATER = "HH:mm";

    public static String format(long timeStamp) {
        return DateFormat.format(TIME_FORMATER, timeStamp).toString();
    }
}
