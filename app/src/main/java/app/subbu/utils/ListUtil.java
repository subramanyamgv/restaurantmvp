package app.subbu.utils;

import android.support.annotation.Nullable;

import java.util.Collection;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public class ListUtil {

    public static boolean isEmptyCollection(@Nullable Collection collection) {
        return collection == null || collection.isEmpty();
    }
}
