package app.subbu.utils;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public class Constants {

    public static final int CACHE_SIZE = 10 * 1024 * 1024; // 10 MiB
}
