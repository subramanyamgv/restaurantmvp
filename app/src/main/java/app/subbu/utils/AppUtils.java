package app.subbu.utils;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Trigger;

import app.subbu.service.ReservationsClearJobService;

import static app.subbu.service.ReservationsClearJobService.JOB_ID;
import static app.subbu.utils.DateTimeUtils.MINUTE;

/**
 * Created by subramanyam.gurajada on 30.07.17.
 */

public class AppUtils {
    public static void initJobDispatcher(FirebaseJobDispatcher jobDispatcher) {
        Job myJob = jobDispatcher.newJobBuilder()
            .setService(ReservationsClearJobService.class) // the JobService that will be called
            .setTag(JOB_ID)        // uniquely identifies the job
            .setRecurring(true)
            .setTrigger(Trigger.executionWindow(9*MINUTE, 10*MINUTE))
            .build();

        jobDispatcher.mustSchedule(myJob);
    }
}
