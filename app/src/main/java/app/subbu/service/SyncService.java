package app.subbu.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import app.subbu.SampleApplication;
import app.subbu.injector.components.ApplicationComponent;
import app.subbu.mvp.model.Customer;
import app.subbu.repository.local.entities.CustomerEntity;
import app.subbu.repository.local.entities.TableMapEntity;
import app.subbu.usecase.ManageCustomersUsecase;
import app.subbu.usecase.ManageTableMapUsecase;
import app.subbu.utils.ListUtil;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by subramanyam.gurajada on 30.07.17.
 */

public class SyncService extends IntentService {

    @Inject
    ManageCustomersUsecase manageCustomersUsecase;
    @Inject
    ManageTableMapUsecase manageTableMapUsecase;

    private ApplicationComponent applicationComponent;
    private CompositeDisposable compositeDisposable;

    public SyncService() {
        super("");
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = ((SampleApplication) getApplication()).getApplicationComponent();
        applicationComponent.inject(this);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        compositeDisposable.add(manageCustomersUsecase
            .getCustomerList()
            .subscribeOn(Schedulers.io())
            .subscribe(this::handleResponseCustomers));

        compositeDisposable.add(manageTableMapUsecase
            .getTableMap()
            .subscribeOn(Schedulers.io())
            .subscribe(this::handleResponseTableMap));
    }

    private void handleResponseCustomers(@Nullable List<Customer> customerList) {
        if (ListUtil.isEmptyCollection(customerList)) {
            return;
        }

        for (Customer customer : customerList) {
            CustomerEntity entity = new CustomerEntity(customer.getId(), customer.getFirstName(),
                customer.getLastName());
            manageCustomersUsecase.putCustomerEntity(entity);
        }
    }

    private void handleResponseTableMap(@Nullable List<Boolean> tableMap) {
        if (ListUtil.isEmptyCollection(tableMap)) {
            return;
        }
        int id = 0;
        for (boolean availability : tableMap) {
            TableMapEntity entity = new TableMapEntity(id++, availability);
            manageTableMapUsecase.putTableMapEntity(entity);
        }
    }
}
