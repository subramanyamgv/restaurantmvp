package app.subbu.service;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import javax.inject.Inject;

import app.subbu.SampleApplication;
import app.subbu.injector.components.ApplicationComponent;
import app.subbu.usecase.ManageTableMapUsecase;

/**
 * Created by subramanyam.gurajada on 30.07.17.
 */

public class ReservationsClearJobService extends JobService {

    public static final String JOB_ID = "reservation_clear_service";

    @Inject
    ManageTableMapUsecase manageTableMapUsecase;

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = ((SampleApplication) getApplication()).getApplicationComponent();
        applicationComponent.inject(this);
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        manageTableMapUsecase.clearTableReservations();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
