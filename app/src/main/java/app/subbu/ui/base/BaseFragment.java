package app.subbu.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.subbu.SampleApplication;
import app.subbu.injector.components.ApplicationComponent;
import butterknife.ButterKnife;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public abstract class BaseFragment extends Fragment {

    protected ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mApplicationComponent = ((SampleApplication) getActivity().getApplication()).getApplicationComponent();

        initDagger();
        initPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        ButterKnife.bind(this, view);
        initialize();
        return view;
    }

    public abstract void initialize();

    public abstract int getLayoutId();

    public abstract void initDagger();

    public abstract void initPresenter();
}
