package app.subbu.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import app.subbu.R;
import app.subbu.SampleApplication;
import app.subbu.injector.components.ApplicationComponent;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Nullable
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    protected ApplicationComponent mApplicationComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());

        ButterKnife.bind(this);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(isDisplayHomeAsUpEnabled());
        }

        mApplicationComponent = ((SampleApplication) getApplication()).getApplicationComponent();

        initDagger();
        initPresenter();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    public void setSubTitle(int resId) {
        if (toolbar != null) {
            getSupportActionBar().setSubtitle(resId);
        }
    }

    protected boolean isDisplayHomeAsUpEnabled() {
        return false;
    }

    public abstract int getLayoutId();

    public abstract void initDagger();

    public abstract void initPresenter();
}
