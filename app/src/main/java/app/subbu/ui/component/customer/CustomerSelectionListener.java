package app.subbu.ui.component.customer;

import app.subbu.repository.local.entities.CustomerEntity;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public interface CustomerSelectionListener {
    void onCustomerSelected(CustomerEntity customerEntity);
}
