package app.subbu.ui.component.table;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import app.subbu.R;
import app.subbu.repository.local.entities.TableMapEntity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public class TableMapAdapter extends RecyclerView.Adapter<TableMapAdapter.TableMapViewHolder> {

    private List<TableMapEntity> tableMapEntities;
    private final TableSelectionListener tableSelectionListener;

    public TableMapAdapter(TableSelectionListener tableSelectionListener) {
        this.tableSelectionListener = tableSelectionListener;
        tableMapEntities = new LinkedList<>();
    }

    @Override
    public TableMapViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.item_table, parent, false);
        return new TableMapViewHolder(view, tableSelectionListener);
    }

    @Override
    public void onBindViewHolder(TableMapViewHolder holder, int position) {
        holder.bind(tableMapEntities.get(position), position);
    }

    @Override
    public int getItemCount() {
        return tableMapEntities.size();
    }


    public void setTableMap(@NonNull List<TableMapEntity> tableMapEntities) {
        this.tableMapEntities = tableMapEntities;
        notifyDataSetChanged();
    }

    public static class TableMapViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ll_table_cell)
        LinearLayout llTableCell;
        @BindView(R.id.txt_cell_id)
        TextView tvCellId;

        private TableSelectionListener listener;

        public TableMapViewHolder(View view, @NonNull TableSelectionListener listener) {
            super(view);
            ButterKnife.bind(this, view);
            this.listener = listener;
        }

        public void bind(TableMapEntity entity, int position) {
            final int backgroundRes = entity.isAvailable() ? R.drawable.background_orange_border : R.drawable.background_orange;
            final int textColorRes = entity.isAvailable() ? R.color.orange : android.R.color.white;

            llTableCell.setBackgroundResource(backgroundRes);
            tvCellId.setText(String.valueOf(position+1));
            tvCellId.setTextColor(ContextCompat.getColor(tvCellId.getContext(), textColorRes));
            itemView.setOnClickListener(v -> listener.onTableSelected(entity));
        }
    }
}
