package app.subbu.ui.component.customer;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import app.subbu.R;
import app.subbu.repository.local.entities.CustomerEntity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public class CustomersAdapter extends RecyclerView.Adapter<CustomersAdapter.CustomersViewHolder> {

    private final CustomerSelectionListener customerSelectionListener;
    private List<CustomerEntity> customerEntities;

    public CustomersAdapter(CustomerSelectionListener customerSelectionListener) {
        this.customerSelectionListener = customerSelectionListener;
        customerEntities = new LinkedList<>();
    }

    @Override
    public CustomersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.item_customer, parent, false);
        return new CustomersViewHolder(view, customerSelectionListener);
    }

    @Override
    public void onBindViewHolder(CustomersViewHolder holder, int position) {
        holder.bind(customerEntities.get(position));
    }

    @Override
    public int getItemCount() {
        return customerEntities.size();
    }

    public void setCustomerEntities(@NonNull List<CustomerEntity> customerEntities) {
        this.customerEntities = customerEntities;
        notifyDataSetChanged();
    }

    public static class CustomersViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_customer_name)
        TextView tvCustomer;

        private CustomerSelectionListener listener;

        public CustomersViewHolder(View view, @NonNull CustomerSelectionListener listener) {
            super(view);
            ButterKnife.bind(this, view);
            this.listener = listener;
        }

        public void bind(@NonNull CustomerEntity customerEntity) {
            final String name = tvCustomer.getResources().getString(R.string.customer_name,
                customerEntity.getFirstName(),
                customerEntity.getLastName());
            tvCustomer.setText(name);
            itemView.setOnClickListener(v -> listener.onCustomerSelected(customerEntity));
        }
    }
}
