package app.subbu.ui.component;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;

import javax.inject.Inject;

import app.subbu.R;
import app.subbu.mvp.presenter.ReservationsPresenter;
import app.subbu.mvp.view.ReservationsView;
import app.subbu.repository.local.entities.CustomerEntity;
import app.subbu.service.SyncService;
import app.subbu.ui.base.BaseActivity;
import app.subbu.ui.component.customer.CustomerFragment;
import app.subbu.ui.component.table.TableMapFragment;

public class ReservationsActivity extends BaseActivity implements ReservationsView {

    @Inject
    ReservationsPresenter reservationsPresenter;

    @Override
    public void navigateToCustomers() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container, CustomerFragment.getInstance(), null);
        transaction.commit();
    }

    @Override
    public void navigateToTableReservation(@NonNull CustomerEntity customerEntity) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, TableMapFragment.getInstance(), null);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void startSyncData() {
        startService(new Intent(this, SyncService.class));
    }

    @Override
    public void setSubTitle(int resId) {
        super.setSubTitle(resId);
    }

    @Override
    public void showProgress(boolean show) {
        //not used
    }

    @Override
    public void showEmpty() {
        //not used
    }

    @Override
    public void showError(Throwable e) {
        //not used
    }

    @Override
    protected boolean isDisplayHomeAsUpEnabled() {
        return true;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_reservation;
    }

    @Override
    public void initDagger() {
        mApplicationComponent.inject(this);
    }

    @Override
    public void initPresenter() {
        reservationsPresenter.attachView(this);
        reservationsPresenter.onCreate();
    }
}
