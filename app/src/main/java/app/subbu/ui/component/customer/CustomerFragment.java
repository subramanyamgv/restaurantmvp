package app.subbu.ui.component.customer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import app.subbu.R;
import app.subbu.mvp.presenter.CustomerPresenter;
import app.subbu.mvp.view.CustomerView;
import app.subbu.mvp.view.ReservationsView;
import app.subbu.repository.local.entities.CustomerEntity;
import app.subbu.ui.base.BaseFragment;
import butterknife.BindView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class CustomerFragment extends BaseFragment implements CustomerView, SearchView.OnQueryTextListener {

    @Inject
    CustomerPresenter customerPresenter;

    @BindView(R.id.rv_table_map)
    RecyclerView rvCustomers;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;
    @BindView(R.id.txt_empty)
    TextView tvEmpty;

    private CustomersAdapter adapter;

    public CustomerFragment() {
    }

    public static CustomerFragment getInstance() {
        return new CustomerFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void initialize() {
        initRecyclerView();
        customerPresenter.onStart();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextChange(String query) {
        customerPresenter.onSearchQuery(query);
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public void onResume() {
        ((ReservationsView) getActivity()).setSubTitle(R.string.select_customer);
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        customerPresenter.onStop();
    }

    @Override
    public void showProgress(boolean show) {
        pbLoading.setVisibility(show ? VISIBLE : GONE);
        rvCustomers.setVisibility(show ? GONE : VISIBLE);
        tvEmpty.setVisibility(GONE);
    }

    @Override
    public void showEmpty() {
        rvCustomers.setVisibility(GONE);
        pbLoading.setVisibility(GONE);
        tvEmpty.setVisibility(VISIBLE);
        tvEmpty.setText(R.string.no_customers);
    }

    @Override
    public void showError(Throwable e) {
        tvEmpty.setVisibility(VISIBLE);
        tvEmpty.setText(R.string.error_get_customers);
    }

    @Override
    public void showCustomerEntities(@NonNull List<CustomerEntity> customerEntities) {
        adapter.setCustomerEntities(customerEntities);
    }

    @Override
    public void navigateToTableReservation(@NonNull CustomerEntity customerEntity) {
        ((ReservationsView) getActivity()).navigateToTableReservation(customerEntity);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_reservation;
    }

    @Override
    public void initDagger() {
        mApplicationComponent.inject(this);
    }

    @Override
    public void initPresenter() {
        customerPresenter.attachView(this);
        customerPresenter.onCreate();
    }

    private void initRecyclerView() {
        adapter = new CustomersAdapter(customerSelectionListener);
        rvCustomers.setLayoutManager(new LinearLayoutManager(getContext()));
        rvCustomers.setAdapter(adapter);
        DividerItemDecoration horizontalDecoration = new DividerItemDecoration(rvCustomers.getContext(),
            DividerItemDecoration.VERTICAL);
        horizontalDecoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));
        rvCustomers.addItemDecoration(horizontalDecoration);
    }

    private final CustomerSelectionListener customerSelectionListener = customerEntity -> {
        customerPresenter.onCustomerSelected(customerEntity);
    };
}
