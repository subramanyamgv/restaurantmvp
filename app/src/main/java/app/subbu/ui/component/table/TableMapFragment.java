package app.subbu.ui.component.table;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import app.subbu.R;
import app.subbu.mvp.presenter.TableMapPresenter;
import app.subbu.mvp.view.ReservationsView;
import app.subbu.mvp.view.TableMapView;
import app.subbu.repository.local.entities.TableMapEntity;
import app.subbu.ui.base.BaseFragment;
import butterknife.BindView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * A placeholder fragment containing a simple view.
 */
public class TableMapFragment extends BaseFragment implements TableMapView {

    private final int GRID_COLUMNS = 5;

    @Inject
    TableMapPresenter tableMapPresenter;

    @BindView(R.id.rv_table_map)
    RecyclerView rvTables;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;
    @BindView(R.id.txt_empty)
    TextView tvEmpty;

    private TableMapAdapter adapter;

    public TableMapFragment() {
    }

    public static TableMapFragment getInstance() {
        return new TableMapFragment();
    }

    @Override
    public void initialize() {
        initRecyclerView();
        tableMapPresenter.onStart();
    }

    @Override
    public void onResume() {
        ((ReservationsView) getActivity()).setSubTitle(R.string.select_table);
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        tableMapPresenter.onStop();
    }

    @Override
    public void showProgress(boolean show) {
        rvTables.setVisibility(show ? GONE : VISIBLE);
        pbLoading.setVisibility(show ? VISIBLE : GONE);
        tvEmpty.setVisibility(show ? GONE : VISIBLE);
    }

    @Override
    public void showEmpty() {
        rvTables.setVisibility(GONE);
        pbLoading.setVisibility(GONE);
        tvEmpty.setVisibility(VISIBLE);
        tvEmpty.setText(R.string.no_table_map);
    }

    @Override
    public void showError(Throwable e) {
        tvEmpty.setVisibility(VISIBLE);
        tvEmpty.setText(R.string.error_get_table_map);
    }

    @Override
    public void showTableMapEntities(List<TableMapEntity> tableMapEntities) {
        adapter.setTableMap(tableMapEntities);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_reservation;
    }

    @Override
    public void initDagger() {
        mApplicationComponent.inject(this);
    }

    @Override
    public void initPresenter() {
        tableMapPresenter.attachView(this);
        tableMapPresenter.onCreate();
    }

    private void initRecyclerView() {
        adapter = new TableMapAdapter(tableSelectionListener);
        rvTables.setLayoutManager(new GridLayoutManager(getContext(), GRID_COLUMNS));
        rvTables.setAdapter(adapter);
    }

    private final TableSelectionListener tableSelectionListener = tableMapEntity -> {
        tableMapEntity.setAvailable(false);
        adapter.notifyDataSetChanged();
        tableMapPresenter.onTableMapSelected(tableMapEntity);
    };
}
