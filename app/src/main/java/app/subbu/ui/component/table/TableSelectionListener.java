package app.subbu.ui.component.table;

import app.subbu.repository.local.entities.TableMapEntity;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public interface TableSelectionListener {
    void onTableSelected(TableMapEntity tableMapEntity);
}
