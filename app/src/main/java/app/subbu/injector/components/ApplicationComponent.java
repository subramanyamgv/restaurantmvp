package app.subbu.injector.components;

import app.subbu.injector.modules.ApplicationModule;
import app.subbu.injector.modules.DbModule;
import app.subbu.injector.modules.NetworkModule;
import app.subbu.injector.scope.PerApplication;
import app.subbu.receiver.StartServiceReceiver;
import app.subbu.service.ReservationsClearJobService;
import app.subbu.service.SyncService;
import app.subbu.ui.component.ReservationsActivity;
import app.subbu.ui.component.customer.CustomerFragment;
import app.subbu.ui.component.table.TableMapFragment;
import dagger.Component;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

@PerApplication
@Component(modules = {ApplicationModule.class, NetworkModule.class, DbModule.class})
public interface ApplicationComponent {

    void inject(ReservationsActivity activity);

    void inject(CustomerFragment fragment);

    void inject(TableMapFragment fragment);

    void inject(SyncService service);

    void inject(ReservationsClearJobService service);

    void inject(StartServiceReceiver receiver);
}
