package app.subbu.injector.modules;

import android.app.Application;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;

import app.subbu.SampleApplication;
import app.subbu.injector.scope.PerApplication;
import dagger.Module;
import dagger.Provides;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */
@Module
public class ApplicationModule {

    private final SampleApplication application;

    public ApplicationModule(SampleApplication application) {
        this.application = application;
    }

    @PerApplication
    @Provides
    public Application provideApplication() {
        return application;
    }

    @PerApplication
    @Provides
    public SampleApplication provideSampleApplication() {
        return application;
    }

    @PerApplication
    @Provides
    public FirebaseJobDispatcher provideJobDispatcher() {
        return new FirebaseJobDispatcher(new GooglePlayDriver(application));
    }
}
