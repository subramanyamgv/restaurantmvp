package app.subbu.injector.modules;

import android.app.Application;
import android.arch.persistence.room.Room;

import app.subbu.injector.scope.PerApplication;
import app.subbu.repository.local.AppDatabase;
import dagger.Module;
import dagger.Provides;

/**
 * Created by subramanyam.gurajada on 30.07.17.
 */

@Module
public class DbModule {

    public static final String DATABASE_NAME = "sample.db";

    @Provides
    @PerApplication
    public AppDatabase provideAppDatabase(Application application) {
        return Room.databaseBuilder(application, AppDatabase.class, DATABASE_NAME).build();
    }
}
