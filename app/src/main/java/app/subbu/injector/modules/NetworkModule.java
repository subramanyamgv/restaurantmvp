package app.subbu.injector.modules;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import app.subbu.BuildConfig;
import app.subbu.injector.scope.PerApplication;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.subbu.utils.Constants.CACHE_SIZE;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

@Module
public class NetworkModule {

    @Provides
    @PerApplication
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    @PerApplication
    Cache provideOkHttpCache(Application application) {
        Cache cache = new Cache(application.getCacheDir(), CACHE_SIZE);
        return cache;
    }

    @Provides
    @PerApplication
    OkHttpClient provideOkHttpClient(Cache cache) {
        return new OkHttpClient.Builder()
            .cache(cache)
            .build();
    }

    @Provides
    @PerApplication
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        String endpointUrl = BuildConfig.apiEndpointUrl;

        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(endpointUrl)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build();

        return retrofit;
    }

}
