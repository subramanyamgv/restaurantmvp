package app.subbu.injector.scope;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */
@Scope
public @Retention(RUNTIME) @interface PerApplication {}
