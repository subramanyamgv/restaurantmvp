package app.subbu.mvp.view;

import android.support.annotation.NonNull;

import java.util.List;

import app.subbu.repository.local.entities.CustomerEntity;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public interface CustomerView extends View {

    void showCustomerEntities(@NonNull List<CustomerEntity> customerEntities);

    void navigateToTableReservation(@NonNull CustomerEntity customerEntity);
}
