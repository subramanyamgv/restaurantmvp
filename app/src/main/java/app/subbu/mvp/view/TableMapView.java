package app.subbu.mvp.view;

import java.util.List;

import app.subbu.repository.local.entities.TableMapEntity;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public interface TableMapView extends View {

    void showTableMapEntities(List<TableMapEntity> tableMapEntities);
}
