package app.subbu.mvp.view;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public interface View {

    void showProgress(boolean show);

    void showEmpty();

    void showError(Throwable e);
}
