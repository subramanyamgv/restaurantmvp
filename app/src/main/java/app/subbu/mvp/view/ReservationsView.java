package app.subbu.mvp.view;

import android.support.annotation.NonNull;

import app.subbu.repository.local.entities.CustomerEntity;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public interface ReservationsView extends View {

    void setSubTitle(int titleRes);

    void navigateToCustomers();

    void navigateToTableReservation(@NonNull CustomerEntity customerEntity);

    void startSyncData();
}
