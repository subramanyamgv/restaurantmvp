package app.subbu.mvp.presenter;

import android.support.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import app.subbu.mvp.view.TableMapView;
import app.subbu.repository.local.entities.TableMapEntity;
import app.subbu.usecase.ManageTableMapUsecase;
import app.subbu.utils.ListUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public class TableMapPresenter implements Presenter<TableMapView> {

    private final ManageTableMapUsecase manageTableMapUsecase;

    private TableMapView tableMapView;
    private CompositeDisposable compositeDisposable;
    private List<TableMapEntity> tableMapEntities;

    @Inject
    public TableMapPresenter(ManageTableMapUsecase manageTableMapUsecase) {
        this.manageTableMapUsecase = manageTableMapUsecase;
    }

    @Override
    public void onCreate() {
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onStart() {
        loadTimeTable();
    }

    @Override
    public void onStop() {
        compositeDisposable.clear();
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(TableMapView view) {
        this.tableMapView = view;
    }

    public void onTableMapSelected(TableMapEntity tableMapEntity) {
        manageTableMapUsecase.updateTableMapEntity(tableMapEntity);
    }

    private void loadTimeTable() {
        tableMapView.showProgress(true);
        compositeDisposable.add(manageTableMapUsecase
            .getTableMapEntities()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::handleResponse, this::handleError));
    }

    private void handleResponse(@Nullable List<TableMapEntity> tableMapEntities) {
        this.tableMapEntities = tableMapEntities;
        tableMapView.showProgress(false);
        if (ListUtil.isEmptyCollection(this.tableMapEntities)) {
            tableMapView.showEmpty();
        } else {
            tableMapView.showTableMapEntities(this.tableMapEntities);
        }
    }

    private void handleError(Throwable error) {
        tableMapView.showProgress(false);
        tableMapView.showError(error);
    }
}
