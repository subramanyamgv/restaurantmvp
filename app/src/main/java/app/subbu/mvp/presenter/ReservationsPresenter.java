package app.subbu.mvp.presenter;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;

import javax.inject.Inject;

import app.subbu.mvp.view.ReservationsView;

import static app.subbu.utils.AppUtils.initJobDispatcher;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public class ReservationsPresenter implements Presenter<ReservationsView> {

    private final FirebaseJobDispatcher jobDispatcher;
    private ReservationsView reservationsView;

    @Inject
    public ReservationsPresenter(FirebaseJobDispatcher jobDispatcher) {
        this.jobDispatcher = jobDispatcher;
    }

    @Override
    public void onCreate() {
        reservationsView.startSyncData();
        reservationsView.navigateToCustomers();
        initJobDispatcher(jobDispatcher);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void attachView(ReservationsView view) {
        this.reservationsView = view;
    }

}
