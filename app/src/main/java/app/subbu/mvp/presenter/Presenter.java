package app.subbu.mvp.presenter;

import app.subbu.mvp.view.View;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public interface Presenter <T extends View> {

    void onCreate();

    void onStart();

    void onStop();

    void onPause();

    void onResume();

    void attachView(T view);
}
