package app.subbu.mvp.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import app.subbu.mvp.view.CustomerView;
import app.subbu.repository.local.entities.CustomerEntity;
import app.subbu.usecase.ManageCustomersUsecase;
import app.subbu.utils.ListUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public class CustomerPresenter implements Presenter<CustomerView> {

    private final ManageCustomersUsecase manageCustomersUsecase;

    private CustomerView customerView;
    private CompositeDisposable compositeDisposable;

    @Inject
    public CustomerPresenter(ManageCustomersUsecase manageCustomersUsecase) {
        this.manageCustomersUsecase = manageCustomersUsecase;
    }

    @Override
    public void onCreate() {
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onStart() {
        loadCustomers("");
    }

    @Override
    public void onStop() {
        compositeDisposable.clear();
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    public void onSearchQuery(String query) {
        loadCustomers(query);
    }

    @Override
    public void attachView(CustomerView view) {
        this.customerView = view;
    }

    public void onCustomerSelected(@NonNull CustomerEntity customerEntity) {
        customerView.navigateToTableReservation(customerEntity);
    }

    private void loadCustomers(@Nullable String query) {
        customerView.showProgress(true);
        compositeDisposable.add(manageCustomersUsecase
            .getCustomerEntities(query)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::handleResponse, this::handleError));
    }

    private void handleResponse(@Nullable List<CustomerEntity> customerEntities) {
        customerView.showProgress(false);
        if (ListUtil.isEmptyCollection(customerEntities)) {
            customerView.showEmpty();
        } else {
            customerView.showCustomerEntities(customerEntities);
        }
    }

    private void handleError(Throwable error) {
        customerView.showProgress(false);
        customerView.showError(error);
    }
}
