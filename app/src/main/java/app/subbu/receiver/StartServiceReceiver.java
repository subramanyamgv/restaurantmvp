package app.subbu.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;

import javax.inject.Inject;

import app.subbu.SampleApplication;
import app.subbu.injector.components.ApplicationComponent;

import static app.subbu.utils.AppUtils.initJobDispatcher;

/**
 * Created by subramanyam.gurajada on 30.07.17.
 */

public class StartServiceReceiver extends BroadcastReceiver {

    @Inject
    FirebaseJobDispatcher jobDispatcher;

    @Override
    public void onReceive(Context context, Intent intent) {
        ApplicationComponent applicationComponent = ((SampleApplication) context.getApplicationContext()).getApplicationComponent();
        applicationComponent.inject(this);
        initJobDispatcher(jobDispatcher);
    }
}
