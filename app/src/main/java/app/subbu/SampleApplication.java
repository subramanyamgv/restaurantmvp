package app.subbu;

import android.app.Application;

import app.subbu.injector.components.ApplicationComponent;
import app.subbu.injector.components.DaggerApplicationComponent;
import app.subbu.injector.modules.ApplicationModule;
import app.subbu.injector.modules.DbModule;
import app.subbu.injector.modules.NetworkModule;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public class SampleApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        setUpInjector();
    }

    private void setUpInjector() {
        applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(new ApplicationModule(this))
            .networkModule(new NetworkModule())
            .dbModule(new DbModule())
            .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
