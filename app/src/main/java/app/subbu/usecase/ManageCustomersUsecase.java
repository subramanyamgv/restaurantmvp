package app.subbu.usecase;

import android.text.TextUtils;

import java.util.List;

import javax.inject.Inject;

import app.subbu.mvp.model.Customer;
import app.subbu.repository.DataRepository;
import app.subbu.repository.local.entities.CustomerEntity;
import io.reactivex.Flowable;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public class ManageCustomersUsecase {

    private DataRepository dataRepository;

    @Inject
    public ManageCustomersUsecase(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
    }

    public Flowable<List<Customer>> getCustomerList() {
        return dataRepository.getCustomerList();
    }

    public void putCustomerEntity(CustomerEntity customerEntity) {
        dataRepository.putCustomer(customerEntity);
    }

    public Flowable<List<CustomerEntity>> getCustomerEntities(String query) {
        return TextUtils.isEmpty(query) ? dataRepository.getCustomerEntities() : dataRepository.searchCustomerEntities(query);
    }
}
