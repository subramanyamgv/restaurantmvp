package app.subbu.usecase;

import java.util.List;

import javax.inject.Inject;

import app.subbu.repository.DataRepository;
import app.subbu.repository.local.entities.TableMapEntity;
import io.reactivex.Flowable;

/**
 * Created by subramanyam.gurajada on 29.07.17.
 */

public class ManageTableMapUsecase {

    private DataRepository dataRepository;

    @Inject
    public ManageTableMapUsecase(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
    }

    public Flowable<List<Boolean>> getTableMap() {
        return dataRepository.getTableMap();
    }

    public Flowable<List<TableMapEntity>> getTableMapEntities() {
        return dataRepository.getTableMapEntities();
    }

    public void putTableMapEntity(TableMapEntity tableMapEntity) {
        dataRepository.putTableMapEntity(tableMapEntity);
    }

    public void updateTableMapEntity(TableMapEntity tableMapEntity) {
        dataRepository.updateTableMapEntity(tableMapEntity);
    }

    public void clearTableReservations() {
        dataRepository.clearTableReservations();
    }
}
