package app.subbu.ui.component;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import app.subbu.mvp.presenter.TableMapPresenter;
import app.subbu.mvp.view.TableMapView;
import app.subbu.usecase.ManageTableMapUsecase;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by subramanyam.gurajada on 30.07.17.
 */

@RunWith(MockitoJUnitRunner.class)
public class TableMapPresenterTest {

    @Mock
    ManageTableMapUsecase mockManageTableMapUsecase;
    @Mock
    TableMapView tableMapView;

    private TableMapPresenter presenter;

    @Before
    public void setup() {
        presenter = new TableMapPresenter(mockManageTableMapUsecase);
        presenter.attachView(tableMapView);
    }

    @Test
    public void testTableMapSelected() {
        presenter.onTableMapSelected(any());
        verify(mockManageTableMapUsecase, times(1)).updateTableMapEntity(any());
    }
}
