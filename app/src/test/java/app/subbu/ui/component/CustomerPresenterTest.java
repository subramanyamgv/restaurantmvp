package app.subbu.ui.component;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import app.subbu.mvp.presenter.CustomerPresenter;
import app.subbu.mvp.view.CustomerView;
import app.subbu.usecase.ManageCustomersUsecase;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by subramanyam.gurajada on 30.07.17.
 */

@RunWith(MockitoJUnitRunner.class)
public class CustomerPresenterTest {

    @Mock
    ManageCustomersUsecase mockManageCustomersUsecase;
    @Mock
    CustomerView customerView;

    private CustomerPresenter presenter;

    @Before
    public void setup() {
        presenter = new CustomerPresenter(mockManageCustomersUsecase);
        presenter.attachView(customerView);
    }

    @Test
    public void testCustomerSelected() {
        presenter.onCustomerSelected(any());
        verify(customerView, times(1)).navigateToTableReservation(any());
    }
}
