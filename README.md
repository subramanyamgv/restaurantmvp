#RestaurantMVP
Restaurant table reservation application using MVP, Retrofit2, RxJava, Dagger2, Room Persistence

[![Download APK](http://www.apkdisc.com/style/apkdiscad2.png)](https://www.dropbox.com/s/ct71a4kuuc6wcgu/app.subbu_release_1.0_1.apk?dl=1)

![](http://i.imgur.com/M2JPLQx.png)
![](http://i.imgur.com/9e03QHb.png)
